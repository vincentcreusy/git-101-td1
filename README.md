#Partie 1 : Travailler en local
##Exercice 1 : Configurer Git
Connaître la version de Git :

    Git version
    
Configurer les variables globales :
    
    git config --global user.name "Vincent Creusy"  
    git config --global user.email "vincentcreusy@gmail.com"

##Exercice 2 : Les 3 états d'un fichier
L'état Git de ces nouveaux fichiers est **untracked**.  
À présent, l'état des deux fichiers est **new file**.  


Initialiser un dossier git :

    git init

Indexer les fichiers :

    git add .
    
Valider les fichiers dans une validation séparée :

    git commit -o .\README.md -m "Commit readme"
    git commit -o .\dev.gradle -m "Commit dev.gradle"
    
Annuler les modifications :

    git reset HEAD~
    
#Partie 2 : Travailler avec un dépôt distant

##Exercice 3 : Pousser son travail vers un dépôt distant

Ajouter un dépôt distant au dépôt local :

    git remote add origin git@gitlab.com:vincentcreusy/git-101-td1.git